﻿
// KeyEventDlg.h: 头文件
//

#pragma once


constexpr int kHotKeySaveAction = 9527;
constexpr int kHotKeyLoadAction = 9528;

// CKeyEventDlg 对话框
class CKeyEventDlg : public CDialogEx
{
// 构造
public:
	CKeyEventDlg(CWnd* pParent = nullptr);	// 标准构造函数

// 对话框数据
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_KEYEVENT_DIALOG };
#endif

	// 生成的消息映射函数
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()

	// save file
	afx_msg void OnBnClickedOk();
	// load backup file
	afx_msg void OnBnClickedCancel();
	afx_msg void OnBnClickedBtnFile();
	afx_msg void OnBnClickedBtnDir();
	afx_msg void OnEnChangeEdit1();
	afx_msg void OnEnChangeEdit2();
	// afx_msg void OnHotKey(UINT nHotKeyId, UINT nKey1, UINT nKey2);

	// 按键处理函数
	void OnGlobalHotKey(int nHotKeyId);

protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV 支持

	HICON m_hIcon;

private:

	CString GetDefaultConfigFile();
	void InitDefaultHotkey();
	void LoadHotKeyConfig();
	void LoadBackupConfig();
	int StringToInt(const CString& s);
	void SetupGlobalHotKey(int id, WORD key, WORD mask);

private:
	// 需要备份的目标文件路径
	CString target_path_;

	// 用于备份文件的目录
	CString backup_dir_;   

	CString config_path_;  // 配置文件全路径
	CString backup_path_;  // 备份文件全路径
	CString prev_backup_;  // 上一份备份文件

	CHotKeyCtrl hotkey_save_;
	CHotKeyCtrl hotkey_load_;
};
