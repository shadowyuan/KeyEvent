#include "pch.h"
#include "hot_key.h"

HotKey::HotKey(int id, intptr_t user)
	: id_(id)
	, user_(user)
	, callback_(nullptr)
	, count_(0)
	, prime_(0) {
	memset(status_, 0, sizeof(status_));
}

HotKey::~HotKey() {}


void HotKey::AppendKey(int vkc, int* status) {
	if (count_ == MAX_KEYS) return;
	status_[count_++] = status;

	if (count_ == 1) {
		prime_ = vkc;
		return;
	}

	if (vkc != VK_SHIFT && vkc != VK_CONTROL && vkc != VK_MENU
		&& vkc != VK_LWIN && vkc != VK_RWIN && (vkc < VK_LSHIFT
		|| vkc > VK_RMENU)) {
		prime_ = vkc;
	}
}

bool HotKey::IsKeyDown() const {
	int result = 0;

	for (int i = 0; i < count_; i++) {
		result += *status_[i];
	}
	return (result == count_);
}

void HotKey::SetCallback(GlobalHotKeyCallback callback) {
	callback_ = callback;
}

void HotKey::ExecuteCallback() { callback_(id_, user_); }

int HotKey::Id() const { return id_; }

int HotKey::PrimeKey() const { return prime_; }
