#pragma once
#include "key_hook.h"

class HotKey {
public:

	static constexpr int KEY_UP = 0;
	static constexpr int KEY_DOWN = 1;

	static constexpr int MAX_KEYS = 5;

	HotKey(int id, intptr_t user);

	~HotKey();

	void AppendKey(int vkc, int *status);

	bool IsKeyDown() const;

	void SetCallback(GlobalHotKeyCallback callback);

	void ExecuteCallback();

	int Id() const;

	int PrimeKey() const;

private:
	int id_;
	intptr_t user_;
	GlobalHotKeyCallback callback_;
	int count_;
	int prime_;
	volatile int* status_[MAX_KEYS];
};

