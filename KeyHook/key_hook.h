#pragma once

#include <stdint.h>

#ifdef KEYHOOK_EXPORTS
#define KEYHOOK_API extern "C" __declspec(dllexport)
#else
#define KEYHOOK_API extern "C" __declspec(dllimport)
#endif // KEYHOOK_EXPORTS


typedef void (*GlobalHotKeyCallback)(int id, intptr_t user);

// 成功返回0
KEYHOOK_API int StartKeyHook();
KEYHOOK_API void StopKeyHook();

/*
 * @brief 注册全局热键，可以是一个按键或多个键的组合键
 * @param id 该组按键的唯一 ID
 * @param vkcode 该组按键的 Virtual-Key Code 数组
 * @param vk_count 该组按键的键个数
 * @param user 用户自定义参数
 * @param callback 该组按键响应时调用的回调函数
 * @return 成功返回0，vkcode无效返回1，vk_count超过限制返回2，
 * id已经存在返回3，未初始化返回4
 */
KEYHOOK_API int RegisterGlobalHotKey(int id, int* vkcode, int vk_count,
                          intptr_t user, GlobalHotKeyCallback callback);
